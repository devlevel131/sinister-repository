<?php

namespace App\EverySystem\Repositories;

use App\EverySystem\Models\Loss;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SinisterRepository extends EloquentRepository
{

    /**
     * SinisterRepository constructor.
     * @param Loss $model
     */
    public function __construct(Loss $model)
    {
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        //mudar
        return $this->model->orderBy('id', 'DESC')->get();
    }

    /**
     * @param array|null $whereFilters
     * @return \Illuminate\Support\Collection
     */
    public function getAllByCompaniesFromUser(array $whereFilters = null)
    {
        $builder = DB::table('losses')
            ->select(['losses.id', 'loss_number', 'loss_body_number', 'insurers_id', 'guilty', 'lawsuit', 'prejuizo_descontado', 'climatic_condition', 'estimated_lost_money', 'total_lost_money', 'climatic_condition', 'date_loss', 'time_loss', 'losses.status_id',
                'vehicle_prefix', 'drivers.name as driver_name', 'vehicles.board', 'companies.name as company_name', 'companies.logo_path as logo',
                'fa_icon', 'icon_color', 'loss_status_types.name as status_name', 'communication_date', 'insurers.name as insurer_name', 'itineraries_id', 'itineraries.line', 'itineraries.itinerary'])
            //->leftJoin('losses_has_loss_documents', 'losses.id', '=', 'losses_has_loss_documents.loss_id')
            //->leftJoin('losses_has_loss_documents', function($join){
              //$join->on('losses.id', '=', 'losses_has_loss_documents.loss_id');
              // ->groupBy('losses.id');
            //})
            ->join('insurers', 'insurers.id', '=', 'losses.insurers_id')
            ->join('itineraries', 'itineraries.id', '=', 'losses.itineraries_id')
            ->join('drivers', 'drivers.id', '=', 'losses.drivers_id')
            ->join('vehicles', 'vehicles.id', '=', 'losses.vehicle_id')
            ->join('companies', 'companies.id', '=', 'losses.companies_id')
            ->join('loss_status_types', 'loss_status_types.id', '=', 'losses.status_id');
            //->orderBy('losses.communication_date', 'DESC');
            //->groupBy('losses.id');
            
            

        // if not admin
        if (!empty($whereFilters)) {
            if (isset($whereFilters['date_de'])){
                $builder->whereDate('losses.date_loss', '>=', $whereFilters['date_de']);
            }
            if (isset($whereFilters['date_ate'])){
                $builder->whereDate('losses.date_loss', '<=', $whereFilters['date_ate']);
            }

            // companies
            if (isset($whereFilters['companies'])) {
                $builder->whereIn('losses.companies_id', $whereFilters['companies']);
            }

            // drivers
            if (isset($whereFilters['driver_filter'])) {
                $builder->whereIn('losses.drivers_id', $whereFilters['driver_filter']);
            }

            // itineraries
            if (isset($whereFilters['itinerary'])) {
                $builder->whereIn('losses.itineraries_id', $whereFilters['itinerary']);
            }
        }
      
        
        //$this->whereCustom($builder, $whereFilters);
        //Log::debug($builder->get());
         Log::info($whereFilters);
      
        $data = $builder->get();
        //dd($data->count());
       
      
        foreach($data as $loss) {
          $Document = DB::table('losses_has_loss_documents')->select(['document_url'])->where([
            ['loss_id', $loss->id]
          ])->whereIn('loss_documents_id', [4, 9])->get();
          
          foreach($Document as $doc) {
            $doc->document_url = str_replace('public/', '', $doc->document_url);
          }
          
          $loss->photos = $Document;
          //Log::debug($Document);
        }
      
        return $data;
    }

    /**
     * @param array $companies
     * @param array $drivers
     * @param array $itineraries
     * @param string $date_de
     * @param string $date_ate
     * @return mixed
     */
    public function returnLocationLosses($companies = [], $drivers = [], $itineraries = [], $date_de = '', $date_ate = '')
    {
        $geos = $this->model->newQuery()->select('id','loss_number','date_loss','vehicle_prefix','loss_body_number','lat', 'lng');

        // companies
        if (count($companies) > 0) {
            $geos->whereIn('companies_id', $companies);
        }

        // drivers
        if (count($drivers) > 0) {
            $ids = [];
            foreach ($drivers as $driv) {
                array_push($ids, $driv);
            }
            $geos->whereIn('drivers_id', $drivers);
        }

        // itineraries
        if (count($itineraries) > 0) {
            $geos->whereIn('itineraries_id', $itineraries);
        }

        // dates from / to
        if ($date_de != '' && $date_ate != '') {
            $geos->whereBetween('date_loss', [$date_de, $date_ate]);
        } elseif ($date_de != '') {
            $geos->whereDate('date_loss', '>=', $date_de);
        } elseif ($date_ate != '') {
            $geos->whereDate('date_loss', '<=', $date_ate);
        }

        return $geos->get();
    }

    public function returnTotalValuesToCompany($company_id, $filters = [])
    {
        $labels = DB::table('companies')->where('id', $company_id)->select('nickname')->first();

        $madrugada = $this->model->where('companies_id', $company_id)->whereBetween('time_loss', ['00:00:00', '06:00:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('companies_id', $company_id)->whereBetween('time_loss', ['06:00:00', '12:00:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('companies_id', $company_id)->whereBetween('time_loss', ['12:00:00', '18:00:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('companies_id', $company_id)->whereBetween('time_loss', ['18:00:00', '00:00:00']);
        $noite = $this->whereCustom($noite, $filters);

        $losses = $this->model->where('companies_id', $company_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('companies_id', $company_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));

        $totalLosses = $this->model->where('companies_id', $company_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);
        
        //Log::debug($totalLosses->count());

        $retorno = [
            'labels' => $labels->nickname,
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    private function whereCustom($query, $filters)
    {
        if ($filters['date_de'] != '' && $filters['date_de'] != 'date_ate') {

            $query = $query->whereBetween('date_loss', [$filters['date_de'], $filters['date_ate']]);
        }
        if (count($filters['companies']) > 0) {
            $query = $query->whereIn('companies_id', $filters['companies']);
        }
        if (count($filters['itineraries']) > 0) {
            $query = $query->whereIn('itineraries_id', $filters['itineraries']);
        }
        if (count($filters['drivers']) > 0) {
            $query = $query->whereIn('drivers_id', $filters['drivers']);
        }

        return $query;
    }

    public function returnTotalValuesToDriver($driver_id, $filters)
    {
        $labels = DB::table('drivers')->where('id', $driver_id)->select('name')->first();

        $madrugada = $this->model->where('drivers_id', $driver_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('drivers_id', $driver_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('drivers_id', $driver_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('drivers_id', $driver_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('drivers_id', $driver_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('drivers_id', $driver_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('drivers_id', $driver_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => $labels->name,
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnTotalValuesToItinerary($itinerary_id, $filters)
    {

        $labels = DB::table('itineraries')->where('id', $itinerary_id)->select(DB::raw("concat(line,' ',itinerary) as name"))->first();

        $madrugada = $this->model->where('itineraries_id', $itinerary_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('itineraries_id', $itinerary_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('itineraries_id', $itinerary_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('itineraries_id', $itinerary_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('itineraries_id', $itinerary_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('itineraries_id', $itinerary_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('itineraries_id', $itinerary_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => $labels->name,
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }
  
    //public function returnTotalValuesToCoversCategorys($covers_id, $filters)

    public function returnTotalValuesToCovers($covers_id, $filters)
    {

        $labels = DB::table('covers')->where('id', $covers_id)->select('name', 'parent_category_id')->first();
      
        if($covers_id > 0) {
          $categoria = DB::table('coverage_parent_categorys')->where('id', $labels->parent_category_id)->select('category_title')->first();
          $categoriaBase = $categoria->category_title;
        } else {
          $categoriaBase = "CATEGORIA DESCONHECIDA";
        }
      
        $madrugada = $this->model->where('covers_id', $covers_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('covers_id', $covers_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('covers_id', $covers_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('covers_id', $covers_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('covers_id', $covers_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('covers_id', $covers_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('covers_id', $covers_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);


        $retorno = [
            'labels' => isset($labels->name) ? $labels->name : '',
            'categoria' => $categoriaBase,
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableBoards($vehicle_id, $filters)
    {

        $labels = DB::table('vehicles')->where('id', $vehicle_id)->select('board')->first();

        $madrugada = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('vehicle_id', $vehicle_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('vehicle_id', $vehicle_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('vehicle_id', $vehicle_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => isset($labels->board) ? $labels->board : '-',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableGuilty($guilty, $filters)
    {

        $madrugada = $this->model->where('guilty', $guilty)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('guilty', $guilty)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('guilty', $guilty)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('guilty', $guilty)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('guilty', $guilty);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('guilty', $guilty);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('guilty', $guilty);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => '',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }
    
    public function returnArrayGraphTableCondutorPrejuizo($prejuizoDescontado, $filters)
    {

        $madrugada = $this->model->where('prejuizo_descontado', $prejuizoDescontado)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('prejuizo_descontado', $prejuizoDescontado)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('prejuizo_descontado', $prejuizoDescontado)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('prejuizo_descontado', $prejuizoDescontado)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('prejuizo_descontado', $prejuizoDescontado);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('prejuizo_descontado', $prejuizoDescontado);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('prejuizo_descontado', $prejuizoDescontado);
        $totalLosses = $this->whereCustom($totalLosses, $filters);
        
        //Log::info('debug inicio');
        //Log::debug($totalLosses->toSql());

        $retorno = [
            'labels' => '',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableInsurer($insurer_id, $filters)
    {
        $labels = DB::table('insurers')->where('id', $insurer_id)->select('name')->first();


        $madrugada = $this->model->where('insurers_id', $insurer_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('insurers_id', $insurer_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('insurers_id', $insurer_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('insurers_id', $insurer_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('insurers_id', $insurer_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('insurers_id', $insurer_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('insurers_id', $insurer_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => $labels->name,
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableBrand($vehicle_id, $filters)
    {
        $labels = DB::table('vehicles')->where('id', $vehicle_id)->select('brand_model')->first();

        $madrugada = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('vehicle_id', $vehicle_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('vehicle_id', $vehicle_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('vehicle_id', $vehicle_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('vehicle_id', $vehicle_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => isset($labels->brand_model) ? $labels->brand_model : '-',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableClerk($clerk_id, $filters)
    {
        $labels = DB::table('users')->where('id', $clerk_id)->select('name')->first();

        $madrugada = $this->model->where('clerk', $clerk_id)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('clerk', $clerk_id)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('clerk', $clerk_id)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('clerk', $clerk_id)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('clerk', $clerk_id);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('clerk', $clerk_id);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('clerk', $clerk_id);
        $totalLosses = $this->whereCustom($totalLosses, $filters);


        $retorno = [
            'labels' => isset($labels->name) ? $labels->name : '-',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }

    public function returnArrayGraphTableDayWeek($day_week, $filters)
    {


        $madrugada = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

//         $tempMed = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week);
//         $tempMed = $this->whereCustom($tempMed, $filters);
//         $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));
      
        // $tempMed = $this->model->whereBetween('time_loss', [$init, $end]);
      
        $tempMed = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss))) as temp_med"));


        $totalLosses = $this->model->where(DB::raw('DAYOFWEEK(date_loss)'), $day_week);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [

            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->first()->temp_med ? $tempMed->first()->temp_med : 0
        ];

        return $retorno;
    }

    public function returnArrayGraphTableTurn($init, $end, $filters, $period)
    {
        
        
        $madrugada = $this->model->whereBetween('time_loss', [$init, $end]);
        $madrugada = $this->whereCustom($madrugada, $filters);


        $losses = $this->model->whereBetween('time_loss', [$init, $end]);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->whereBetween('time_loss', [$init, $end]);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss))) as temp_med"));


        $totalLosses = $this->model->whereBetween('time_loss', [$init, $end]);
        $totalLosses = $this->whereCustom($totalLosses, $filters);
      
          //Log::debug($madrugada->count());


        $retorno = [

            'total_losses' => $totalLosses->count(),
            'madrugada' => $period == 'madrugada' ? $madrugada->count() : 0,
            'manha' => $period == 'manha' ? $madrugada->count() : 0,
            'tarde' => $period == 'tarde' ? $madrugada->count() : 0,
            'noite' => $period == 'noite' ? $madrugada->count() : 0,
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->first()->temp_med ? $tempMed->first()->temp_med : 0
        ];

        return $retorno;
    }

    public function renderStreetsUnique()
    {
        return $this->model->select('street')->distinct()->get();
    }

    /**
     * @return mixed
     */
    public function renderPrefixUnique()
    {
        return $this->model->select('vehicle_prefix')->distinct()->get();
    }

    public function returnArrayGraphTableLocal($locale, $filters)
    {
        $labels = $this->model->where('street', $locale);
        $labels = $this->whereCustom($labels, $filters)->select('street')->first();


        $madrugada = $this->model->where('street', $locale)->whereBetween('time_loss', ['00:00:00', '06:59:00']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('street', $locale)->whereBetween('time_loss', ['07:00:00', '12:59:00']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('street', $locale)->whereBetween('time_loss', ['13:00:00', '18:59:00']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('street', $locale)->whereBetween('time_loss', ['18:00:00', '00:59:00']);
        $noite = $this->whereCustom($noite, $filters);


        $losses = $this->model->where('street', $locale);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('street', $locale);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw("round(avg(DATEDIFF(communication_date,date_loss)))"));


        $totalLosses = $this->model->where('street', $locale);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => isset($labels->street) ? $labels->street : '-',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->count()
        ];

        return $retorno;
    }
  
    public function createDateRange($startDate, $endDate, $format = "Y/m") {
        $begin = new \DateTime($startDate);
        $end = new \DateTime($endDate);

        $interval = new \DateInterval('P1D'); // 1 month
        $dateRange = new \DatePeriod($begin, $interval, $end);

        $range = [];
        foreach ($dateRange as $date) {
            $range[] = $date->format($format);
        }

        return $range;
    }

    public function returnTotalValuesToMonth($companies = [], $drivers = [], $itineraries = [], $date_de = '', $date_ate = ''){
      $graph = [];
      $added = [];
      $periods = [];
      $ranges = $this->createDateRange($date_de, $date_ate);
      
      foreach ($ranges as $range) {
        $explodeRange = explode('/', $range);
        
        if(!in_array($range, $added)) {
          array_push($periods, [
            'numericYear' => $explodeRange[0],
            'range' => $range,
            'numericMonth' => $explodeRange[1],
            'month' => $this->returnMonth($explodeRange[1]),
            'total' => 0
          ]);
          
          array_push($added, $range);
        }
      }
      
      foreach ($periods as $period) {
        $data = $this->model
          ->whereYear('date_loss', $period['numericYear'])
          ->whereMonth('date_loss', $period['numericMonth']);
        
        if (count($companies) > 0) {
          $data = $data->whereIn('companies_id', $companies);
        }
        
        if (count($drivers) > 0) {
          $data = $data->whereIn('drivers_id', $drivers);
        }
        
        if (count($itineraries) > 0) {
          $data = $data->whereIn('itineraries_id', $itineraries);
        }
        
        if ($date_de != '' && $date_ate != '') {
          $data = $data->whereBetween('date_loss', [$date_de, $date_ate]);
        } elseif ($date_de != '') {
          $data = $data->whereDate('date_loss', '>=', $date_de);
        } elseif ($date_ate != '') {
          $data = $data->whereDate('date_loss', '<=', $date_ate);
        }
        
        $period['total'] = $data->count();
        array_push($graph, $period);
      }
     

        //Log::debug($interval->y);
        //Log::debug($date_ate);

//         for ($x = 1; $x <= 12; $x++) {

//             if ($x + 10) {
//                 $x = '0' . $x;
//             }
//             $data = $this->model->whereMonth('date_loss', $x);
//             if (count($companies) > 0) {
//                 $data = $data->whereIn('companies_id', $companies);
//             }
//             if (count($drivers) > 0) {
//                 $data = $data->whereIn('drivers_id', $drivers);
//             }
//             if (count($itineraries) > 0) {
//                 $data = $data->whereIn('itineraries_id', $itineraries);
//             }
//             if ($date_de != '' && $date_ate != '') {
//                 $data = $data->whereBetween('date_loss', [$date_de, $date_ate]);
//             } elseif ($date_de != '') {
//                 $data = $data->whereDate('date_loss', '>=', $date_de);
//             } elseif ($date_ate != '') {
//                 $data = $data->whereDate('date_loss', '<=', $date_ate);
//             }
//             array_push($graph, [
//                 'month' => self::returnMonth($x),
//                 'data' => $data->count(),
//             ]);
//         }

        return $graph;
    }

    private function returnMonth($month)
    {
        switch ($month) {
            case '01':
                return 'JAN';
                break;
            case '02':
                return 'FEV';
                break;
            case '03':
                return 'MAR';
                break;
            case '04':
                return 'ABR';
                break;
            case '05':
                return 'MAI';
                break;
            case '06':
                return 'JUN';
                break;
            case '07':
                return 'JUL';
                break;
            case '08':
                return 'AGO';
                break;
            case '09':
                return 'SET';
                break;
            case '10':
                return 'OUT';
                break;
            case '11':
                return 'NOV';
                break;
            case '12':
                return 'DEZ';
                break;
            default:
                # code...
                break;
        }
    }


    public function renderByFilterPrefix($companies, $drivers, $itineraries, $date_de, $date_ate, $prefix)
    {
        $retorno = $this->model;

        if (count($companies) > 0) {
            $retorno = $retorno->whereIn('companies_id', $companies);
        }

        if (count($drivers) > 0) {
            $retorno = $retorno->whereIn('drivers_id', $drivers);
        }
        if (count($itineraries) > 0) {
            $retorno = $retorno->whereIn('itineraries_id', $itineraries);
        }

        if ($date_de != '' && $date_ate != '') {
            $retorno = $retorno->whereBetween('date_loss', [$date_de, $date_ate]);
        }
        if ($prefix == 'condutor') {
            $retorno = $retorno->select('drivers_id')->groupBy('drivers_id');
        }
        if ($prefix == 'itinerarios') {
            $retorno = $retorno->select('itineraries_id')->groupBy('itineraries_id');
        }
        if ($prefix == 'tipo_sinistro') {
            $retorno = $retorno->select('covers_id')->groupBy('covers_id');
        }
        if ($prefix == 'placa' || $prefix == 'marcas') {
            $retorno = $retorno->select('vehicle_id')->groupBy('vehicle_id');
        }
        if ($prefix == 'local') {
            $retorno = $retorno->select('street')->groupBy('street');
        }
        if ($prefix == 'prefix') {
            $retorno = $retorno->select('vehicle_prefix')->groupBy('vehicle_prefix');
        }
        if ($prefix == 'empresa') {
            $retorno = $retorno->select('companies_id')->groupBy('companies_id');
        }
        if ($prefix == 'cia_seguro') {
            $retorno = $retorno->select('insurers_id')->groupBy('insurers_id');
        }

        if ($prefix == 'atendentes') {
            $retorno = $retorno->select('clerk')->groupBy('clerk');
        }

        return $retorno->get()->toArray();
    }

    public function returnArrayGraphTablePrefix($locale, $filters)
    {
        $labels = $this->model->where('vehicle_prefix', $locale);
        $labels = $this->whereCustom($labels, $filters)->select('vehicle_prefix')->first();

        $madrugada = $this->model->where('vehicle_prefix', $locale)->whereBetween('time_loss', ['00:00:00', '06:59:59']);
        $madrugada = $this->whereCustom($madrugada, $filters);

        $manha = $this->model->where('vehicle_prefix', $locale)->whereBetween('time_loss', ['07:00:00', '12:59:59']);
        $manha = $this->whereCustom($manha, $filters);

        $tarde = $this->model->where('vehicle_prefix', $locale)->whereBetween('time_loss', ['13:00:00', '17:59:59']);
        $tarde = $this->whereCustom($tarde, $filters);

        $noite = $this->model->where('vehicle_prefix', $locale)->whereBetween('time_loss', ['18:00:00', '23:59:59']);
        $noite = $this->whereCustom($noite, $filters);

        $losses = $this->model->where('vehicle_prefix', $locale);
        $losses = $this->whereCustom($losses, $filters);

        $colisao = DB::table('losses_has_accident_claims')->where('accident_claims_id', 1)->whereIn('losses_id', $losses->pluck('id')->toArray());

        $tempMed = $this->model->where('vehicle_prefix', $locale);
        $tempMed = $this->whereCustom($tempMed, $filters);
        $tempMed = $tempMed->select(DB::raw('round(avg(DATEDIFF(communication_date,date_loss))) as avg_time'));

        $totalLosses = $this->model->where('vehicle_prefix', $locale);
        $totalLosses = $this->whereCustom($totalLosses, $filters);

        $retorno = [
            'labels' => isset($labels->vehicle_prefix) ? $labels->vehicle_prefix : '-',
            'total_losses' => $totalLosses->count(),
            'madrugada' => $madrugada->count(),
            'manha' => $manha->count(),
            'tarde' => $tarde->count(),
            'noite' => $noite->count(),
            'colisao' => $colisao->count(),
            'temp_med' => $tempMed->first()->avg_time
        ];

        return $retorno;
    }

}